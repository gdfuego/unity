---
title: "Privacy Focus"
date: "2020-08-15T13:07:57-04:00"
weight: "20"
menu: "main"
draft: "false"
---

	I made Steve Bannon’s psychological warfare tool.
	  - Christopher Wylie (2018)

{{< youtube id="oHp7XiJrWkA" autoplay="false" >}}

	Commercial television delivers 20 million people a minute.
	In commercial broadcasting the viewer pays for the privilege of having himself sold.
	It is the consumer who is consumed.
	You are the product of t.v.
	You are delivered to the advertiser who is the customer.
	He consumes you.
	The viewer is not responsible for programming——
	You are the end product.
	  - Richard Serra and Carlota Fay Schoolman (1973)

{{< youtube nbvzbj4Nhtk >}}

